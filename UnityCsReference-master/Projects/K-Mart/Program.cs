﻿using System;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int j = 0;
            while(j<20)
            {
                string str = Console.ReadLine();
                if(str !="")
                {
                    string output = testvalue(str);
                    j = j + 1;
                }
                else
                {
                    break;
                }
            }
        }

        public static string testvalue(string str)
        {
            int[] valuesplit = str.Split(' ').Select(n => Convert.ToInt32(n)).ToArray();
            int oldvalue = 0;
            int indicator = 0;
            int newvalue = 0;
            string storevalue = "";
            int checklength = 0;
            for (int i = 0; i < valuesplit.Length - 1; i++)
            {
                if (valuesplit[i] < valuesplit[i + 1])
                {
                    oldvalue = valuesplit[i];
                    storevalue += oldvalue.ToString() + " ";
                    indicator = 1;
                    newvalue = newvalue + 1;
                }
                else
                {
                    if (indicator == 1)
                    {
                        storevalue += valuesplit[i];
                        indicator = 0;
                        if (checklength < newvalue)
                        {
                            checklength = newvalue;
                            newvalue = 0;
                            str = storevalue;
                            storevalue = "";
                        }
                    }

                }
                if (valuesplit.Length - 2 == i && indicator == 1)
                {
                    storevalue += valuesplit[i + 1];
                    indicator = 0;
                    if (checklength < newvalue)
                    {
                        checklength = newvalue;
                        newvalue = 0;
                        str = storevalue;
                        storevalue = "";
                    }
                }
              
            }
            Console.WriteLine(str);
            return (str);
        }

    }
}
